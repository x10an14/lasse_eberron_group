# Second session

## Etter kampen der nede
Den våkne broren overgir seg og bistår oss i å ta ned warforgen.

Healer redder goblin og shifter som er nede etter kampen.
Vi lar shifterne gå. etter de har gitt oss ID papirer.

Shifteren som overgav seg bærer både broren og goblinen opp, og lover å gå til politiet.

## En dør
Vi finner en dør lengre inn i gangen hvor bakholdet fant sted.
Liar, Min og jeg utforsker døra.

Når jeg bestemmer meg for å se nærmere på døra (face in front of door), så får jeg og Min syre i fjeset.
Prøver å åpne døra, finner ut at å putte boka fra den avdøde onkelen (som har samme logo/tegning som matchet et stort emblem pmidt i den store runde døra).

## Døren åpner seg
Det er en rørsjakt somgår horisontalt rett nedover.
Vi finner ut at den er 70 fot ned.
Vi klatrer ned med 2x 50fot silk rope og grappling hook.

Der nede er det en vei/passasje, ganske bred.
En ende er stengt av et steinras.
I den andre enden er det et nytt hull, veldig mørkt.

## Hullet i veien
Ny vei bare 2-3m ned, men vi måtte lyse for å se noe som helst.
Min super-hero-landing gikk til helvete.

Jeg og Min blir heala.

Her nede så hører vi maaaange flere insekter.
Vi går mot insektene.
Lyden av insektene blir høyere og høyere. Den blir plagsom.

Men så stabiliserer den.

## Stort rom
Når vi følger brosteinen videre, så slutter tunnelen plutselig.

Der inne ser jeg ruiner et stykke unna.
Når jeg begynner å gå mot dem, så angriper biller mine biologiske kompanjonger.

## Vi overlever såvidt
Og finner et tempel som insektene holder seg unna.
Vi blir natten over, levler opp og healer opp.

Cora får 3x heal potions, gir en til hver av de to andre (ikke meg).

## Vi utforsker videre
Og finner i nord-østre hjørne smien.

Vi finner to bikkjer inni der.
Vi tar ned bikkjene etter en kamp.

Vi finner mange ting under oljehyrer (oil cloths).
Jostein (Min) tok notater av alt vi fant.

Vi ser deretter tre høl som mathcer pinnene fra bikkjene.

Etter å putte de i riktig rekkefølge inn i matchende høl over smien, så finner vi det vi er ute etter pluss mer saker Jostein har skrevet ned.
