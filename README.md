# Lasse's Eberron DnD 3.5 group

This repo contains the datafiles and logs pertaining to my character in this roleplaying group.
The roleplaying group consists of the following members:

| Member	| Character	    | Race	| Package	|
|---------------|-------------------|:---------:|:-------------:|
| Lasse		| GM; N/A	    | GM; N/A	| GM; N/A	|
| Christian	| ???		    | Warforged	| Fighter	|
| Prienth (Pri)	| Liar		    | Elf	| Ranger	|
| Jostein	| Minharath (Min)   | Human(?)	| Psion		|
| Vittorio	| 	| 	| 	|
| Olea		| Cora		    | Halfling	| Healer	|

## Tools used in this repo
 - Git
 - LibreOffice Calc
   - [Ref. this stackexchange answer](https://softwareengineering.stackexchange.com/a/181522).

